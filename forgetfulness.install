<?php

/**
 * @file
 * Install, update and uninstall functions for the Forgetfulness module.
 */

/**
 * Implements hook_schema().
 *
 * @return array
 */
function forgetfulness_schema() {
  $schema['forgetfulness'] = array(
    'description'     => 'Progress records.',
    'fields'          => array(
      'fid' => array(
        'description' => 'The primary identifier for a record',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'The {user}.uid.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'nid' => array(
        'description' => 'The {node}.nid.',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
      ),
      'result' => array(
        'description' => 'Test result.',
        'type'        => 'int',
        'size'        => 'tiny',
        'not null'    => TRUE,
        'unsigned'    => FALSE,
        'default'     => '0',
      ),
      'reminder' => array(
        'description' => 'Next reminder.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the record was created.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the record was most recently changed.',
        'type'        => 'int',
        'not null'    => TRUE,
        'default'     => 0,
        'unsigned'    => TRUE,
      ),
      'suggestion' => array(
        'description' => 'Used in reminder.',
        'type'        => 'varchar',
        'not null'    => FALSE,
      ),
    ),
    'primary key'     => array('fid'),
    'indexes' => array(
      'uid' => array('uid'),
      'nid' => array('nid'),
    ),
  );
  return $schema;
}

/**
 * Add suggestion to a reminder
 */
function forgetfulness_update_7001() {
  db_add_field('forgetfulness', 'suggestion', array('type' => 'varchar', 'length' => 255));
}

// vim: set filetype=php expandtab tabstop=2 shiftwidth=2 autoindent smartindent:
